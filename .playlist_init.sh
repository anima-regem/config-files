#!/bin/bash

function sp_dark() {
  bash ~/Music/Playlists/play_dark_stormy.sh
}
function sp_good() {
  bash ~/Music/Playlists/play_feeling_good.sh
}
function sp_heart() {
  bash ~/Music/Playlists/play_heart_beats.sh
}
function sp_lofi() {
  bash ~/Music/Playlists/play_lofi.sh
}
function sp_nature() {
  bash ~/Music/Playlists/play_nature.sh
}
function sp_fav() {
  bash ~/Music/Playlists/play_fav.sh
}
function sp_50() {
  bash ~/Music/Playlists/play_gb_50.sh
}
function sp_study() {
  bash ~/Music/Playlists/play_intense_study.sh
}
function sp_lonely() {
  bash ~/Music/Playlists/play_lonely.sh
}
function sp_piano() {
  bash ~/Music/Playlists/play_piano.sh
}



